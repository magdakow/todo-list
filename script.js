let $list;
let myInput;
let popupInput;
let addTodoBtn;
let form;
let modalForm;
let currentId;
let loader;
let modal;

function main() {
    prepareDOMElements();
    prepareDOMEvents();
    getTodoElementsFromServer();
}

function prepareDOMElements() {
    //to będzie miejsce do pobrania naszych elementów z drzewa DOM i zapisanie ich w zmiennych
    $list = document.getElementById('list');
    myInput = document.querySelector('#myInput');
    popupInput = document.querySelector('#popupInput');
    addTodoBtn = document.querySelector('#addTodo');
    form = document.querySelector('#addForm');
    modalForm = document.querySelector('#editForm');
    loader = document.querySelector('.loader-box');
    modal = document.getElementById("myModal");
}

function prepareDOMEvents() {
    //przygotowanie litenerow
    $list.addEventListener('click', listClickManager);
    form.addEventListener('submit', addNewTodoViaForm);
    modalForm.addEventListener('submit', updateElementTextViaForm);
    modal.addEventListener('click', modalClickManager);
}

function addNewTodoViaForm(e) {
    e.preventDefault();
    addNewTodo();
}

function addNewTodo() {
    if (myInput.value.trim() !== '') {
        // addNewElementToList(myInput.value);
        axios.post('http://195.181.210.249:3000/todo/', {title: myInput.value}).then(response => {
            console.log('Added ' + myInput.value + 'to the list. Response: ' + response);
            refreshList();
        }).catch(error => {
            console.log(error);
        });
        myInput.value = '';
    }
}

function addNewElementToList(title, id) {
    //obsługa dodawania elementów do listy
    //$list.appendChild(createElement('nowy', 2))
    const newElement = createElement(title, id);
    $list.appendChild(newElement);
}

function createElement(title, id) { /*Title, author, id */
    //tworzyc reprezentacje DOM elementu return new element
    const newElement = document.createElement('li');
    newElement.dataset.id = id;

    const newSpan = document.createElement('span');
    newSpan.innerText = title;
    newElement.appendChild(newSpan);

    const spanForButtons = document.createElement('span');

    //pobranie id elementu np do usuniecia
    const markBtn = document.createElement('button');
    markBtn.innerText = 'Done';
    markBtn.className = 'done';
    markBtn.id = id;
    spanForButtons.appendChild(markBtn);

    const editBtn = document.createElement('button');
    editBtn.innerText = 'Edit';
    editBtn.className = 'edit';
    editBtn.id = id;
    spanForButtons.appendChild(editBtn);

    const deleteBtn = document.createElement('button');
    deleteBtn.innerText = 'Delete';
    deleteBtn.className = 'delete';
    deleteBtn.id = id;
    spanForButtons.appendChild(deleteBtn);

    spanForButtons.className="spanButtons";

    newElement.appendChild(spanForButtons);
    return newElement;
}

function getTodoElementsFromServer() {
    showLoader();
    axios.get('http://195.181.210.249:3000/todo/').then(response => {
        console.log(response);
        response.data.forEach(todo => {
            if (todo.title !== "") {
                addNewElementToList(todo.title, todo.id);
            } else {
                // deleteListElement(todo.id);
            }
        });
    }).catch(error => {
        console.log(error);
    }).finally(() => {
        hideLoader();
    });
}

function showLoader() {
    loader.classList.add('loader-box--show');
}

function hideLoader() {
    loader.classList.remove('loader-box--show');
}

function listClickManager(event) {
    //rozstrzygnięcie co dokładnie zostało kliknięte i wywołanie odpowiedniej funkcji
    // event.target.parentElement.id
    if (event.target.className === 'edit') {
        editListElement(event.target.id)
    } else if (event.target.className === 'delete') {
        deleteListElement(event.target.id)
    } else if (event.target.className === 'done') {
        markDoneListElement(event.target.id)
    }
}

function editListElement(id) {
    openPopup(id);
    currentId = id;
}

function deleteListElement(id) {
    console.log("Delete id:" + id);
    axios.delete('http://195.181.210.249:3000/todo/' + id)
        .then(response => {
            console.log('Deleted successfully' + response);
        })
        .catch(error => {
            console.log(error);
        });
    console.log(document.querySelector('li[data-id="' + id + '"]'));
    $list.removeChild(document.querySelector('li[data-id="' + id + '"]'));
}

function markDoneListElement(id) {
    console.log(id);
}

function modalClickManager(event) {
    if (event.target.id === 'closePopup' || event.target.id === 'closeTodo') {
        closePopup();
    } else if (event.target.id === 'saveTodo') {
        if (popupInput.value.trim() !== '') {
            updateElementText();
        }
    }
}

function updateElementText() {
    let editedSpanText = document.querySelector('li[data-id="' + currentId + '"]').firstChild;
    let oldText = editedSpanText.innerText;
    let newText = editedSpanText.innerText = popupInput.value;

    axios.put('http://195.181.210.249:3000/todo/' + currentId, {title: popupInput.value}).then(() => {
        console.log('Updated text from ' + oldText + ' to ' + newText + '.');
        refreshList();
    }).catch(error => {
        console.log(error);
    });

    closePopup();
    currentId = '';
    popupInput.value = '';
}

function updateElementTextViaForm(e) {
    e.preventDefault();
    if (popupInput.value.trim() !== '') {
        updateElementText();
    }
}

function refreshList() {
    clearList();
    getTodoElementsFromServer();
}

function clearList() {
    document.getElementById("list").innerHTML = "";
}

function openPopup(id) {
    modal.style.display = "block";
    let editedSpanText = document.querySelector('li[data-id="' + id + '"]').firstChild;
    popupInput.value = editedSpanText.innerText;
}

function closePopup() {
    modal.style.display = "none";
}

document.addEventListener('DOMContentLoaded', main);